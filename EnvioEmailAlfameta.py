import pyodbc
import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


#versao 0.1


def log(msglog):
    with open('LogEnvioEmail.log', 'a', encoding='utf-8') as arquivo:
        arquivo.write(time.strftime("%d/%m/%Y %X") + ": " + msglog + "\n")
        arquivo.close()


def EnviaEmail(email, senha, host, porta, destinatario, assunto, mensagem):
    # Configura content
    msg = MIMEMultipart('alternative')
    msg['Subject'] = assunto
    msg['To'] = destinatario
    msg['From'] = email
    HTML = MIMEText(mensagem, 'html')
    msg.attach(HTML)
    # Envio
    try:
        servidor = smtplib.SMTP(host, porta)
        servidor.ehlo()
        servidor.starttls()
        servidor.login(email, senha)
    except Exception as e:
        log("Erro ao conectar no servidor SMTP. Mensagem Original: " + str(e))
    servidor.sendmail(email, destinatario, msg.as_string())


def ConsultaSQL():
    try:
        conexao = pyodbc.connect("DSN=Alfameta")
    except Exception as e:
        log("Erro no driver ODBC. Mensagem Original: " + str(e))
    cursor = conexao.cursor()
    cursor.execute("select d.CodEmpresa, "
                   "d.CodEnvioEmail,"
                   "d.CodCliente, "
                   "lower(d.Email) as Email, "
                   "d.Assunto, "
                   "d.Mensagem,"
                   "E.EmailHost,"
                   "E.EmailPorta,"
                   "E.EmailUsername,"
                   "E.EmailPassword from dba.EnvioEmailAutomatico as d "
                   "join Empresa as E on(d.CodEmpresa = E.CodEmpresa) "
                   "where d.Situacao = 'P';")
    retorno = cursor.fetchall()
    return retorno


def AtualizaRetorno(registro):
    try:
        conexao = pyodbc.connect("DSN=Alfameta")
    except Exception as e:
        log("Erro no driver ODBC. Mensagem Original: " + str(e))
    cursor = conexao.cursor()
    print(registro)
    cursor.execute("update EnvioEmailAutomatico set Situacao = '" + str(registro[2]) + "', RetornoErro = '" +
                    str(registro[3]).replace("'", "") + "', DataEnvio = '" + registro[4] + "', HoraEnvio = '" +
                    registro[5] + "' where CodEnvioEmail = " + str(registro[1]) + ";")
    cursor.commit()
    cursor.close()


def Main():
    log("Iniciando processo de envio de emails")
    results = ConsultaSQL()
    total = 0
    erros = 0
    ok = 0
    for consulta in results:
        enviado = ()
        try:
            total += 1
            EnviaEmail(consulta[8], consulta[9], consulta[6], consulta[7], consulta[3], consulta[4], consulta[5])
        except Exception as e:
            erros += 1
            enviado = (consulta[0], consulta[1], 'E', str(e), time.strftime("%Y-%m-%d"), time.strftime("%H:%M"))
            pass
        else:
            ok += 1
            enviado = (consulta[0], consulta[1], 'R', 'ENVIADO COM SUCESSO!', time.strftime("%Y-%m-%d"),
                             time.strftime("%H:%M"))
            pass
        finally:
            AtualizaRetorno(enviado)
            pass

    log("Envio de emails concluido. Total: " + str(total) + ". Ok: " + str(ok) + ". Erros: " + str(erros) + ".")


Main()
#pyinstaller --onefile --noconsole "C:\Documentos\Outros Arquivos\Gabriel\EnvioEmailAfameta\EnvioEmailAlfameta\EnvioEmailAlfameta.py"
